---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: netbootd-api
  labels:
    app.kubernetes.io/name: netbootd-api
  annotations:
    reloader.stakater.com/auto: 'true'
spec:
  replicas: 3
  selector:
    matchLabels:
      app.kubernetes.io/name: netbootd-api
  template:
    metadata:
      labels:
        app.kubernetes.io/name: netbootd-api
    spec:
      containers:
        - name: netbootd-api
          image: registry.gitlab.com/lenitech/docker/netbootd:v0.14.4@sha256:54f9727266042af75ef2d2413a9a10ce5beb539eaf1c6016714f77e80d34cebd
          imagePullPolicy: IfNotPresent
          args:
            - --trace
            - api
            - --listen-address=0.0.0.0
            - --listen-port=8080
          env:
            - name: NETBOOTD_API_AUTHORIZATION
              valueFrom:
                secretKeyRef:
                  key: api-key
                  name: netbootd
                  optional: false
            - name: NETBOOTD_NETBOXURL
              valueFrom:
                secretKeyRef:
                  key: url
                  name: netbootd-netbox
                  optional: true
            - name: NETBOOTD_NETBOXAPIKEY
              valueFrom:
                secretKeyRef:
                  key: api-key
                  name: netbootd-netbox
                  optional: true
            - name: NETBOOTD_POWERDNSURL
              valueFrom:
                secretKeyRef:
                  key: url
                  name: netbootd-powerdns
                  optional: true
            - name: NETBOOTD_POWERDNSAPIKEY
              valueFrom:
                secretKeyRef:
                  key: api-key
                  name: netbootd-powerdns
                  optional: true
            - name: NETBOOTD_DHCPDDEFAULTPREFIX
              valueFrom:
                secretKeyRef:
                  key: prefix
                  name: netbootd-dhcpd
                  optional: true
            - name: NETBOOTD_DHCPDDEFAULTRANGE
              valueFrom:
                secretKeyRef:
                  key: range
                  name: netbootd-dhcpd
                  optional: true
            - name: NETBOOTD_DHCPDDEFAULTROUTER
              valueFrom:
                secretKeyRef:
                  key: router
                  name: netbootd-dhcpd
                  optional: true
            - name: NETBOOTD_DHCPDDEFAULTDNS
              valueFrom:
                secretKeyRef:
                  key: dns
                  name: netbootd-dhcpd
                  optional: true
            - name: NETBOOTD_DHCPDDEFAULTLEASEDURATION
              valueFrom:
                secretKeyRef:
                  key: lease-duration
                  name: netbootd-dhcpd
                  optional: true
          ports:
            - containerPort: 8080
              name: http
              protocol: TCP
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /health/live
              port: http
              scheme: HTTP
            periodSeconds: 1
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /health/ready
              port: http
              scheme: HTTP
            periodSeconds: 5
            successThreshold: 1
            timeoutSeconds: 1
          resources:
            requests:
              cpu: 10m
              memory: 64Mi
            limits:
              memory: 64Mi
          securityContext:
            runAsUser: 1000
            runAsNonRoot: true
            allowPrivilegeEscalation: false
            seccompProfile:
              type: RuntimeDefault
            capabilities:
              drop:
                - ALL
      dnsPolicy: ClusterFirstWithHostNet
      serviceAccountName: netbootd-api
      securityContext:
        fsGroup: 1000
        runAsUser: 1000
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/name: netbootd-api
                topologyKey: kubernetes.io/hostname
              weight: 1
